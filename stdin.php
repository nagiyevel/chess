<?php
/* Define STDIN in case if it is not already defined by PHP for some reason */
if (!defined("STDIN")) {
	define("STDIN", fopen('php://stdin', 'r'));
}
$me = "    chess: ";
echo $me, "Date: ", date('d.m.Y h:i:s'), "\n";
echo $me, "Enter the size of the board in format axb or press enter for default 8x8\n";
while (true) {
	$line = trim(fgets(STDIN));
	++$a;
	if ($line == '') {
		$matrix_size = [8, 8];
		break;
	} elseif (!preg_match('/[0-9]x[0-9]/', $line)) {
		echo $me, "Please, enter the size of the board in format axb or press enter for default 8x8\n";
		continue;
	}
	$matrix_size = explode('x', $line);
	break;
}
echo $me, "{$matrix_size[0]}x{$matrix_size[1]} chess board initialized \n";
echo $me, "Type command \n";

require_once "Chess.php";
$chess = new Chess($matrix_size);
while (true) {
	$line = trim(fgets(STDIN));
	$parameter = explode(' ', $line);
	$cmd = $parameter[0];
	array_shift($parameter);

	switch ($cmd) {
		case 'exit':
		case 'quit':
		case 'bye':
		case '0':
			echo $me, "bye\n";
			break 2;
		case 'insert':
			$action = $chess->insert('Figure' . $parameter[0], $parameter[1], $parameter[2]);
			echo $me, $action['message'], "\n";
			break;
		case 'move':
			$action = $chess->move($parameter[0], $parameter[1], $parameter[2], $parameter[3]);
			echo $me, $action['message'], "\n";
			break;
		case 'remove':
			$action = $chess->remove($parameter[0], $parameter[1]);
			echo $me, $action['message'], "\n";
			break;
		case 'status':
			$action = $chess->status();
			echo $me, $action['message'], "\n";
			break;
		case 'save':
			switch ($parameter[0]) {
				case 'file':
					require_once "ChessFileMemoryAdapter.php";
					$adapter = new ChessFileMemoryAdapter();
					break;
				case 'redis':
					require_once "ChessRedisMemoryAdapter.php";
					$adapter = new ChessRedisMemoryAdapter();
					break;
				default:
					echo $me, 'Memory adapter not found' . "\n";
					break 2;
			}
			$action = $adapter->save($chess, $parameter[1]);
			echo $me, $action['message'], "\n";
			break;
		case 'load':
			switch ($parameter[0]) {
				case 'file':
					require_once "ChessFileMemoryAdapter.php";
					$adapter = new ChessFileMemoryAdapter();
					break;
				case 'redis':
					require_once "ChessRedisMemoryAdapter.php";
					$adapter = new ChessRedisMemoryAdapter();
					break;
				default:
					echo $me, 'Memory adapter not found' . "\n";
					break 2;
			}
			$action = $adapter->load($chess, $parameter[1], '');
			echo $me, $action['message'], "\n";
			break;
		default:
			echo $me, "Command not found\n";
			echo $me, 'cmd:', $cmd, "\n";
			echo $me, 'params:', implode(',', $parameter), "\n";
			echo $me, "Type command \n";
			break;
	}
}
