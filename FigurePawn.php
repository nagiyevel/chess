<?php
/**
 * Pawn.php
 * Author: Elchin Nagiyev <elchin at nagiyev.pro>
 * Date: 2/12/2018 14:47
 */
require_once "ChessFigure.php";

class FigurePawn extends ChessFigure {
	public function __construct() {
		parent::__construct();
	}

	protected function canMove($to_x, $to_y) {
		$can_move = parent::canMove($to_x, $to_y);
		if ($can_move['status']) {
			if ($to_x == $this->position[0] && $to_y == $this->position[0] + 1) {
				return ['status' => true, 'message' => ""];
			} else {

				return ['status' => false, 'message' => $this->type . " can't move to {$to_x},{$to_y}\n"];
			}

		} else {
			return $can_move;
		}
	}
}
