To run script type in cli

    php stdin.php

To insert figures type in cli `insert {figure type} {x coord} {y coord}`.
For example:

    insert Pawn 1 1
    insert King 1 2
    insert Rock 1 3
To remove figure in `{x coord} {y coord}` use `remove {x coord} {y coord}`.
For example:

remove 1 1

To move figure from `{x coord} {y coord}` to `{x2 coord} {y2 coord}` use `move {x coord} {y coord} {x2 coord} {y2 coord}`
For example:

    move 1 1 1 3

To vew board status use `status` command.
To save board status use `save {memory adapter} {status id}` command.  Two memory adapters supported: file, redis. `id` is the identification number for cache naming. 
For example:

    save file 1
    save redis 1
 
To load board from memory use `load  {memory adapter} {status id}` Two memory adapters supported: file, redis. `id` is the identification number for cache naming. 

    load redis 1
    load file 1

To stop script use `quit` or `exit` or type `0`.
![CLI example](./Screenshot_3.png)



