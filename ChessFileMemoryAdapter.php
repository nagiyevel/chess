<?php
/**
 * ChessFileMemoryAdapter.php
 * Author: Elchin Nagiyev <elchin at nagiyev.pro>
 * Date: 2/12/2018 16:57
 */

require_once "ChessMemoryAdapter.php";

class ChessFileMemoryAdapter extends ChessMemoryAdapter {
	public $dir = './';

	public function __construct() {
		parent::__construct();
	}

	public function load(Chess $chess, $id, $cache) {
		$file = $this->dir . "chess_{$id}.json";
		if (file_exists($file)) {
			$cache = file_get_contents($file);
			parent::load($chess, $id, $cache);

			return ['status' => true, 'message' => "Status loaded from $file with id $id.\n"];
		}

		return ['status' => false, 'message' => "Can't load $file.\n"];
	}

	public function save(Chess $chess, $id) {
		$cache = parent::save($chess, $id);
		$file = $this->dir . "chess_{$id}.json";
		$fp = fopen($file, "w");
		if (flock($fp, LOCK_EX)) {  // acquire an exclusive lock
			fwrite($fp, $cache);
			flock($fp, LOCK_UN);    // release the lock

			return ['status' => true, 'message' => "Status saved to $file with id $id.\n"];
		}

		return ['status' => false, 'message' => "Can't save.\n"];

	}

}
