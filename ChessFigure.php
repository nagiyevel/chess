<?php
/**
 * ChessFigure.php
 * Author: Elchin Nagiyev <elchin at nagiyev.pro>
 * Date: 2/12/2018 12:21
 */

class ChessFigure {
	protected $position = [];
	protected $type;
	public static $desc_size = [];

	public function __construct() {
		$this->type = get_class($this);
	}

	public function moveTo($to_x, $to_y) {
		$can_move = $this->canMove($to_x, $to_y);
		if ($can_move['status']) {
			$current_position = $this->position;
			$this->position = [$to_x, $to_y];

			return ['status' => true, 'message' => $this->type . " moved from {$current_position[0]},{$current_position[1]} to $to_x,$to_y\n"];
		} else {
			return $can_move;
		}
	}

	protected function canMove($to_x, $to_y) {
		if ($to_x > self::$desc_size[0] || $to_x < 1 || $to_y > self::$desc_size[1] || $to_y < 1) {
			return ['status' => false, 'message' => "Out of desk " . self::$desc_size[0] . "," . self::$desc_size[1] . "\n"];
		}

		return ['status' => true, 'message' => ""];
	}

	public function remove() {
		return ['status' => true, 'message' => $this->type . " removed from {$this->position[0]},{$this->position[1]}\n"];
	}

	public function insert($x, $y) {
		if ($x > self::$desc_size[0] || $y > self::$desc_size[1]) {
			return ['status' => false, 'message' => "Out of desk " . self::$desc_size[0] . "," . self::$desc_size[1] . "\n"];
		}

		$this->position = [$x, $y];

		return ['status' => true, 'message' => "{$this->type} inserted to {$x},{$y}.\n"];
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	public function getStatus() {
		return ['type' => $this->type, 'x' => $this->position[0], 'y' => $this->position[1]];
	}

}
