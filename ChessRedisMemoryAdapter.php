<?php
/**
 * ChessRedisSaveAdapter.php
 * Author: Elchin Nagiyev <elchin at nagiyev.pro>
 * Date: 2/12/2018 16:30
 */
require 'Predis/Autoloader.php'
require_once "Chess.php";

class ChessRedisMemoryAdapter extends ChessMemoryAdapter {

	/**
	 * ChessRedisSaveAdapter constructor.
	 */
	public function __construct() {
		parent::__construct();
		$single_server = ['scheme'   => 'tcp',
		                  'host'     => '',
		                  'port'     => 6379,
		                  'password' => ''
		];
		if (empty(self::$instance['Predis'])) {
			try {
				self::$instance['Predis'] = new Predis\Client($single_server);
			} catch (Exception $e) {
				echo "Couldn't connected to Redis";
				echo $e->getMessage();
				die();
			}
		}
	}

	public function load(Chess $chess, $id, $cache) {
		if (self::$instance['Predis']->exists("chess_" . $id)) {
			$cache = self::$instance['Predis']->get("chess_" . $id);
			parent::load($chess, $id, $cache);

			return ['status' => true, 'message' => "Status loaded from Redis with id $id.\n"];
		}

		return ['status' => false, 'message' => "Can't load Redis.\n"];
	}

	public function save(Chess $chess, $id) {
		$cache = parent::save($chess, $id);
		self::$instance['Predis']->set("chess_" . $id, $cache);

		return ['status' => true, 'message' => "Status saved to Redis with id $id.\n"];
	}
}
