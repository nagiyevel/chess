<?php
/**
 * ChessSaveAdapter.php
 * Author: Elchin Nagiyev <elchin at nagiyev.pro>
 * Date: 2/12/2018 16:27
 */
require_once "Chess.php";

class ChessMemoryAdapter {
	public static $instance;

	public function __construct() {
	}

	public function save(Chess $chess, $id) {
		return json_encode($chess->getStatus());
	}

	public function load(Chess $chess, $id, $cache) {
		$status = json_decode($cache, true);
		$chess->load($status);
	}

	public function __destruct() {
	}
}
