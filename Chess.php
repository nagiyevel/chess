<?php
/**
 * Chess.php
 * Author: Elchin Nagiyev <elchin at nagiyev.pro>
 * Date: 2/12/2018 12:10
 */

require_once "ChessFigure.php";

class Chess {
	private $size = [8, 8];
	private $desc = [];

	public function __construct($size = null) {
		if (!is_null($size)) {
			$this->size = $size;
		}
		ChessFigure::$desc_size = $this->size;
	}

	public function insert($figure, $x, $y) {
		$obj_name = $figure;
		if (!file_exists($obj_name . '.php')) {
			return ['status' => false, 'message' => "$figure not found\n"];
		}
		require_once $obj_name . '.php';
		if (empty($this->desc[$x][$y])) {
			/** @var ChessFigure $obj_name */
			$obj = new $obj_name();
			$action = $obj->insert($x, $y);
			if ($action['status']) {
				$this->desc[$x][$y] = $obj;
			}

			return $action;
		} else {
			return ['status' => false, 'message' => "Can't add $figure to {$x},{$y}. {$this->desc[$x][$y]->getType()} is in this position.\n"];
		}
	}

	public function move($x, $y, $to_x, $to_y) {
		$obj = $this->desc[$x][$y];
		if (empty($obj)) {
			return ['status' => false, 'message' => "{$x},{$y} is empty.\n"];
		} elseif (empty($this->desc[$to_x][$to_y])) {
			/** @var ChessFigure $obj */
			$action = $obj->moveTo($to_x, $to_y);
			if ($action['status'] === true) {
				$this->desc[$to_x][$to_y] = $obj;
				unset($this->desc[$x][$y]);
			}

			return $action;
		} else {
			return ['status' => false, 'message' => "Can't move figure from $x,$y to $to_x,$to_y. {$this->desc[$x][$y]->getType()} is in this position.\n"];
		}

	}

	public function getStatus() {
		$arr = [];
		$arr['size'] = $this->size;
		foreach ($this->desc as $obj_list) {
			foreach ($obj_list as $obj) {
				$arr['figure'][] = $obj->getStatus();
			}
		}

		return $arr;
	}

	public function remove($x, $y) {
		$obj = $this->desc[$x][$y];
		if (empty($obj)) {
			return ['status' => false, 'message' => "{$x},{$y} is empty.\n"];
		} else {
			/** @var ChessFigure $obj */
			$action = $obj->remove();
			if ($action['status'] === true) {
				unset($this->desc[$x][$y]);
			}

			return $action;
		}
	}

	public function status() {
		$arr = $this->getStatus();
		//var_dump($this->desc);

		return ['status' => true, 'message' => json_encode($arr) . "\n"];
	}

	public function load($status) {
		unset($this->desc);

		$this->size = $status['size'];

		foreach ($status['figure'] as $figure) {
			$this->insert($figure['type'], $figure['x'], $figure['y']);
		}

		return true;
	}
}
